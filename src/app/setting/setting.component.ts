import { Component, OnInit } from "@angular/core";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { ControlPanelService } from "../_services/control-panel.service";
enum trans {
  false,
  true,
}
@Component({
  selector: "app-setting",
  templateUrl: "./setting.component.html",
  styleUrls: ["./setting.component.css"],
})
export class SettingComponent implements OnInit {
  autoRenew = 0;
  trans = trans;
  constructor(private controlPanelService: ControlPanelService) {}

  ngOnInit() {}
  updateManualScheduleModeFlag(id, e) {
    this.controlPanelService
      .updateManualScheduleModeFlag(id, e.checked)
      .subscribe((result) => this.controlPanelService.setControlPanels());
  }
  changeTheme(event) {
    let style = document.documentElement.style;
    switch (event) {
      case 1: {
        style.setProperty("--main-color", "#011627");
        style.setProperty("--accent-color", "#e71d36");
        style.setProperty("--main-deep-color", "#242423");
        style.setProperty("--base-color", "#fdfffc");
        style.setProperty("--base-deep-color", "#f8f8f8");
        break;
      }
      case 2: {
        style.setProperty("--main-color", "#4f5d75");
        style.setProperty("--main-deep-color", "#2d3142");
        style.setProperty("--accent-color", "#ef8354");
        style.setProperty("--base-color", "#ffffff");
        style.setProperty("--base-deep-color", "#bfc0c0");
        break;
      }
      case 3: {
        style.setProperty("--main-color", "#70c1b3");
        style.setProperty("--main-deep-color", "#247ba0");
        style.setProperty("--accent-color", "#ff1654");
        style.setProperty("--base-color", "#f3ffbd");
        style.setProperty("--base-deep-color", "#b2dbbf");
        break;
      }
      case 4: {
        style.setProperty("--main-color", "#14213d");
        style.setProperty("--main-deep-color", "#000000");
        style.setProperty("--accent-color", "#fca311");
        style.setProperty("--base-color", "#ffffff");
        style.setProperty("--base-deep-color", "#e5e5e5");
        break;
      }
      case 5: {
        style.setProperty("--main-color", "#457b9d");
        style.setProperty("--main-deep-color", "#1d3557");
        style.setProperty("--accent-color", "#e63946");
        style.setProperty("--base-color", "#f1faee");
        style.setProperty("--base-deep-color", "#a8dadc");
        break;
      }
      case 6: {
        style.setProperty("--main-color", "#143601");
        style.setProperty("--main-deep-color", "#14213d");
        style.setProperty("--accent-color", "#fca311");
        style.setProperty("--base-color", "#ffffff");
        style.setProperty("--base-deep-color", "#e5e5e5");
        break;
      }
      case 7: {
        style.setProperty("--main-color", "#306773");
        style.setProperty("--main-deep-color", "#7da63f");
        style.setProperty("--accent-color", "#000");
        style.setProperty("--base-color", "#b5d932");
        style.setProperty("--base-deep-color", "#6099a6");
        break;
      }
    }
  }
}
