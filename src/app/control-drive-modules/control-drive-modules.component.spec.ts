import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlDriveModulesComponent } from './control-drive-modules.component';

describe('ControlDriveModulesComponent', () => {
  let component: ControlDriveModulesComponent;
  let fixture: ComponentFixture<ControlDriveModulesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlDriveModulesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlDriveModulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
