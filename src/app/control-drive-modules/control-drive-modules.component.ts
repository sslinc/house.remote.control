import { Component, OnInit } from "@angular/core";
import { actuators } from "../_models/actuators";
import { ControlPanelService } from "../_services/control-panel.service";
import { ActuatorService } from "../_services/actuator.service";
import { ActivatedRoute } from "@angular/router";
@Component({
  selector: "app-control-drive-modules",
  templateUrl: "./control-drive-modules.component.html",
  styleUrls: ["./control-drive-modules.component.css"],
})
export class ControlDriveModulesComponent implements OnInit {
  modules = actuators;
  constructor(
    private route: ActivatedRoute,
    private controlPanelService: ControlPanelService,
    private actuatorService: ActuatorService
  ) {
    this.controlPanelService.setControlPanels();
  }

  ngOnInit() {}
  controlPanelChanged(e) {
    this.controlPanelService.changeControlPanel(e);
  }
}
