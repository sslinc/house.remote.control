import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatDividerModule } from "@angular/material/divider";

import { ActuatorControlHistoryService } from "../_services/actuator-control-history.service";
import { ControlPanelService } from "../_services/control-panel.service";

import { ActuatorControlHistory } from "../_models/actuator-control-history";

enum operations {
  "정지",
  "작동",
  "완료",
}
@Component({
  selector: "actuator-control-history",
  templateUrl: "./actuator-control-history.component.html",
  styleUrls: ["./actuator-control-history.component.css"],
})
export class ActuatorControlComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private actuatorControlHistoryService: ActuatorControlHistoryService,
    private controlPanelService: ControlPanelService
  ) {}
  operations = operations;
  startDate = new Date();
  endDate = new Date();
  options1 = {
    fieldSeparator: ",",
    quoteStrings: '"',
    decimalseparator: ".",
    showLabels: true,
    headers: ["operation", "house_id", "target", "datetime", "note", "range"],
    showTitle: true,
    title: "histories",
    useBom: true,
    removeNewLines: true,
    keys: ["operation", "house_id", "target", "datetime", "note", "range"],
  };

  ngOnInit() {
    this.getControlHistory();
  }
  ngDoCheck() {
    // this.histories[0].id = 5;
  }
  getControlHistory() {
    const id = +this.route.snapshot.paramMap.get("id");
    let a = ActuatorControlHistoryService.getDateFormat(
      this.startDate,
      "start"
    );
    let b = ActuatorControlHistoryService.getDateFormat(this.endDate, "end");
    this.actuatorControlHistoryService.getControlHistory(id, a, b);
  }
  downloadFile(data: ActuatorControlHistory[]) {
    // let data1 = JSON.stringify(data);
    // console.log(data1);
    // const blob = new Blob([data1], { type: "text/csv" });
    // console.log(blob);
    // const url = window.URL.createObjectURL(blob);
    // window.open(url);
  }
  downloadHistory() {
    // const id = +this.route.snapshot.paramMap.get("id");
    // let a = ActuatorControlService.getDateFormat(this.startDate);
    // let b = ActuatorControlService.getDateFormat(this.endDate);
    // this.actuatorControlService
    //   .getControlHistory(id, 0, a, b)
    //   .subscribe(data => this.downloadFile(data)), //console.log(data),
    //   error => console.log("Error downloading the file."),
    //   () => console.info("OK");
  }
  clickSearchHistory() {
    this.getControlHistory();
  }
}
