import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { ActuatorControlComponent } from "./actuator-control-history.component";

describe("ActuatorControlLogComponent", () => {
  let component: ActuatorControlComponent;
  let fixture: ComponentFixture<ActuatorControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ActuatorControlComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActuatorControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
