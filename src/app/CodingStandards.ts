// 1. 이름 규칙 및 스타일
//   1.1. 클래스와 구조체에 파스칼 케이싱 사용
class PlayerManager {
  // 1.2. 지역변수이름과 파라미터이름에 카멜 케이싱 사용
  public someMethod(someParameter: number): void {
    someNumber: Number;
  }

  // 1.3. 메소드에 동사-객체 쌍 사용
  // 1.4. 메소드 이름에 카멜 케이싱을 사용
  // 1.5. 리턴값이 있는 메소드는 리턴 된 값을 설명하는 이름을 사용

  public getAge(): Number {
    return 0;
  }
}
//   1.6. 상수에 대해 밑줄과 대문자를 사용
//   1.7. 정적변수에 대해 밑줄과 대문자를 사용
const SOME_CONSTANT: Number = 1;
//   1.8. 열거형에 접두사 E를 사용
enum EDirection {
  North,
  South
}
//   1.9. 부울 변수에 접두사 b, 부울 속성에 접두사 is를 사용
bFired: Boolean;
IsFired: Boolean;
//   1.10. 지역 변수를 사용중인 첫번째 줄에 최대한 가깝게 선언
//   1.11. 클래스 변수 및 메소드의 순서
//    public variables/properties
//    internal variables/properties
//    protected variables/properties
//    private variables
//    constructors
//    public methods
//    Internal methods
//    protected methods
//    private methods
class Example {
  // 1.12. 대부분의 경우 함수 오버로딩을 피해야합니다
  //Use:
  public GetAnimByIndex(index: Number): Number {
    return 0;
  }
  public GetAnimByName(name: string): Number {
    return 0;
  }

  //Instead of:
  public GetAnim(index: Number): Number {
    return 0;
  }
  public GetAnim1(name: string): Number {
    return 0;
  }
  // 1.13. 한 줄에 하나의 변수 만 선언
  //BAD:
  //counter:number = 0, index:number = 0;

  //GOOD:
  counter: number = 0;
  index: number = 0;

  // 1.14. 함수에서 널이 리턴되는 경우 함수 이름 뒤에 OrNull
  // 1.15. null 매개 변수를 사용하는 경우 OrNull로 매개 변수 이름 접미사
  public GetNameOrNull(nameOrNull: string): string {
    return null;
  }
}
