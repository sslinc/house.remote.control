import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManualControlModeComponent } from './manual-control-mode.component';

describe('ManualControlModeComponent', () => {
  let component: ManualControlModeComponent;
  let fixture: ComponentFixture<ManualControlModeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManualControlModeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManualControlModeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
