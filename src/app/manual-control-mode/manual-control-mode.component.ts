import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ActuatorService } from "../_services/actuator.service";
import { ManualControlService } from "../_services/manual-control.service";
import { ControlPanelService } from "../_services/control-panel.service";
import { WeatherService } from "../_services/weather.service";
@Component({
  selector: "app-manual-control-mode",
  templateUrl: "./manual-control-mode.component.html",
  styleUrls: ["./manual-control-mode.component.css"],
})
export class ManualControlModeComponent implements OnInit {
  action_range: number[] = new Array();
  constructor(
    private route: ActivatedRoute,
    private actuatorService: ActuatorService,
    private manualControlService: ManualControlService,
    private controlPanelService: ControlPanelService,
    private weatherService: WeatherService
  ) {}
  onSelectRange(id, event) {
    this.action_range[id] = event.target.value.replace(/[^0-9]/g, "");
  }
  openWindowControl(id: number) {
    if (!this.action_range[id]) this.action_range[id] = 0;
    this.manualControlService
      .setCommand(
        id,
        this.action_range[id] + "",
        this.controlPanelService.CurrentControlPanel.id
      )
      .subscribe((result) => console.log(result));
    this.weatherService.openSnackBarWithHumidity(
      this.controlPanelService.CurrentControlPanel.lat,
      this.controlPanelService.CurrentControlPanel.lng
    );
    // .subscribe(result => console.log(result));
  }
  closeWindowControl(id: number) {
    this.manualControlService.setCommand(
      id + 1,
      this.action_range[id] + "",
      this.controlPanelService.CurrentControlPanel.id
    );
    this.weatherService.openSnackBarWithHumidity(
      this.controlPanelService.CurrentControlPanel.lat,
      this.controlPanelService.CurrentControlPanel.lng
    );
    // .subscribe(result => console.log(result));
  }
  onControl(id: number) {
    this.manualControlService
      .setCommand(id, "1", this.controlPanelService.CurrentControlPanel.id)
      .subscribe((result) => console.log(result));
  }
  offControl(id: number) {
    this.manualControlService
      .setCommand(id, "0", this.controlPanelService.CurrentControlPanel.id)
      .subscribe((result) => console.log(result));
  }
  ngOnInit() {
    // this.getModule();
  }
}
