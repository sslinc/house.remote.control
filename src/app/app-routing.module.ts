import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AutoManualModeComponent } from "./auto-manual-mode/auto-manual-mode.component";
import { ActuatorControlComponent } from "./actuator-control-history/actuator-control-history.component";
import { LoginComponent } from "./login/login.component";
import { AutoControlModeComponent } from "./auto-control-mode/auto-control-mode.component";
import { TotalStateViewerComponent } from "./total-state-viewer/total-state-viewer.component";
import { SettingComponent } from "./setting/setting.component";
import { CheckingComponent } from "./checking/checking.component";

const routes: Routes = [
  // { path: "", redirectTo: "total/state", pathMatch: "full" },
  { path: "controller/:id", component: AutoManualModeComponent },
  { path: "control/log/:id", component: ActuatorControlComponent },
  { path: "auto/control", component: AutoControlModeComponent },
  { path: "total/state", component: TotalStateViewerComponent },
  { path: "setting", component: SettingComponent },
  { path: "login", component: LoginComponent },
  { path: "", component: CheckingComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: "reload" })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
