import { Component, OnInit } from "@angular/core";
import { AccountService } from "../_services/account.service";
import { Router, ActivatedRoute } from "@angular/router";
import { ControlPanelService } from "../_services/control-panel.service";
import { Location } from "@angular/common";
@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  private account;
  private password;
  constructor(
    private accountService: AccountService,
    private controlPanelService: ControlPanelService,
    private router: Router,
    private location: Location
  ) {}

  ngOnInit() {}

  login() {
    this.accountService.logIn(this.account, this.password).subscribe((user) => {
      this.accountService.User = user;
      this.controlPanelService.setControlPanels();
      // this.accountService
      //   .setUser()
      //   .subscribe((result) => (this.accountService.User = result));
      // this.controlPanelService.setControlPanels();
      // if (user) this.router.navigate(["/controller", 0]);
    });
    setTimeout(() => {
      this.router.navigate(["/total/state"]);
    }, 200);
  }
}
