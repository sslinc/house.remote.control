import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import {
  MatDividerModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatSnackBarModule,
  MatSlideToggleModule,
} from "@angular/material";
import { MatSelectModule } from "@angular/material/select";
import { HttpClientModule } from "@angular/common/http";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { TopBarComponent } from "./top-bar/top-bar.component";
import { ControlDriveModulesComponent } from "./control-drive-modules/control-drive-modules.component";
import { AutoManualModeComponent } from "./auto-manual-mode/auto-manual-mode.component";
import { ManualControlModeComponent } from "./manual-control-mode/manual-control-mode.component";
import { ActuatorControlComponent } from "./actuator-control-history/actuator-control-history.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Angular2CsvModule } from "angular2-csv";
import { LoginComponent } from "./login/login.component";
import { UserStateComponent } from "./user-state/user-state.component";
import { AutoControlModeComponent } from "./auto-control-mode/auto-control-mode.component";
import { TotalStateViewerComponent } from "./total-state-viewer/total-state-viewer.component";
import { BottomNavigationComponent } from "./bottom-navigation/bottom-navigation.component";
import { SettingComponent } from "./setting/setting.component";
import { CheckingComponent } from './checking/checking.component';
@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    ControlDriveModulesComponent,
    AutoManualModeComponent,
    ManualControlModeComponent,
    ActuatorControlComponent,
    LoginComponent,
    UserStateComponent,
    AutoControlModeComponent,
    TotalStateViewerComponent,
    BottomNavigationComponent,
    SettingComponent,
    CheckingComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,

    HttpClientModule,

    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatDividerModule,
    MatSelectModule,
    MatSnackBarModule,
    MatSlideToggleModule,

    FormsModule,
    Angular2CsvModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
