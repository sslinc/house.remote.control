import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { catchError, map, tap } from "rxjs/operators";
import { Observable, of } from "rxjs";

import { ActuatorControlHistory } from "../_models/actuator-control-history";
import { ControlPanel } from "../_models/control-panel";

import { ServerInformation } from "../_classes/server-information";

@Injectable({
  providedIn: "root",
})
export class NetworkService {
  private controlServerHost = ServerInformation.getControlServerHost();
  private controlCommandUrl = "/plc/command/state/write";
  private controlStateUrl = "/actuator/current/status";
  private controlHistoryUrl = "/actuator/control/history/actuator";

  constructor(private http: HttpClient) {}

  setCommand(id: number, command: string, control_panel_id: number) {
    return this.http
      .post(this.controlServerHost + this.controlCommandUrl, {
        state: command,
        code_id: id,
        controlpanel_id: control_panel_id,
      })
      .pipe(catchError(this.handleError("setCommand", [])));
  }
  getCurrentState(id: number, control_panel_id: number) {
    const url = `http://220.82.139.172:3000/plctest/plc/controlpanel/${control_panel_id}/acturator/${id}/code/${id}/state/read`;
    return this.http
      .get(url)
      .pipe(catchError(this.handleError("getCurrentState", [])));
  }

  getControlHistory(
    id: number,
    control_panel_id: number,
    start_date: string,
    end_date: string
  ): Observable<ActuatorControlHistory[]> {
    const url = `${this.controlHistoryUrl}/${id}/control_panel/${control_panel_id}/start/${start_date}/end/${end_date}`;
    return this.http
      .get<any>(this.controlServerHost + url, {
        withCredentials: true,
      })
      .pipe(
        catchError(
          this.handleError<ActuatorControlHistory[]>("getControlHistory", [])
        )
      );
  }

  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      if (error.status == 401) alert("로그인이 필요한 서비스 입니다.");
      else if (error.status == 403) alert("권한이 없는 사용자 입니다.");
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
