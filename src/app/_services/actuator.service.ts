import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { Actuator } from "../_models/actuator";
import { debounceTime, catchError, map, tap } from "rxjs/operators";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ServerInformation } from "../_classes/server-information";
interface State {
  id: number;
  state: string;
}
@Injectable({
  providedIn: "root",
})
export class ActuatorService {
  private controlServerHost = ServerInformation.getControlServerHost();
  private actuatorsUrl = "/actuator/actuators/house";
  private updateActuatorDurationUrl = "/actuator/duration";
  actuatorList;
  currentActuator: Actuator;
  actuators = new Array();
  state: string[] = ["0", "0"];
  constructor(private http: HttpClient) {}
  getActuatorList(control_panel_id) {
    return this.http
      .get<Actuator[]>(
        `${this.controlServerHost}/actuator/actuators/${control_panel_id}`,
        {
          withCredentials: true,
        }
      )
      .pipe(catchError(this.handleError<Actuator[]>("getActuators")));
  }
  setActuators(parent_id: number): void {
    this.actuators = [];
    this.state = [];
    this.actuatorList.forEach((element) => {
      if (element.parent_id == parent_id) {
        this.actuators.push(element);
        this.getCurrentState(
          element.control_panel_id,
          element.state_code
        ).subscribe((result) => this.state.push(result.state));
      }
    });
  }
  getCurrentState(control_panel_id: number, id: number): Observable<State> {
    const url = `/plc/controlpanel/${control_panel_id}/code/${id}/state/read`;
    return this.http
      .get<State>(this.controlServerHost + url)
      .pipe(catchError(this.handleError<State>("getCurrentState")));
  }
  getCurrentActuator(id: number): Observable<Actuator> {
    return of(this.currentActuator);
  }
  updateActuatorDuration(actuator_id: number, duration: number) {
    const url = `${this.updateActuatorDurationUrl}/${actuator_id}`;
    return this.http
      .post(
        this.controlServerHost + url,
        {
          duration: duration,
        },
        {
          withCredentials: true,
        }
      )
      .pipe(catchError(this.handleError("updateActuatorDuration", [])));
  }
  activate() {}
  inactivate() {}
  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      // console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
