import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { catchError, map, tap } from "rxjs/operators";
import { Observable, of } from "rxjs";
import { ActuatorControlHistory } from "../_models/actuator-control-history";
import { User } from "../_models/user";
import { NetworkService } from "./network.service";
import { ControlPanelService } from "./control-panel.service";

@Injectable({
  providedIn: "root",
})
export class ActuatorControlHistoryService {
  histories: ActuatorControlHistory[];
  public static getDateFormat(date: Date, type: string) {
    let formatedDate =
      date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
    if (type == "end")
      formatedDate =
        date.getFullYear() +
        "-" +
        (date.getMonth() + 1) +
        "-" +
        (date.getDate() + 1);
    return formatedDate;
  }

  constructor(
    private http: HttpClient,
    private networkService: NetworkService,
    private controlPanelService: ControlPanelService
  ) {}
  getControlHistory(id: number, startDate, endDate) {
    this.networkService
      .getControlHistory(
        id,
        this.controlPanelService.CurrentControlPanel.id,
        startDate,
        endDate
      )
      .subscribe((histories) => (this.histories = histories));
  }
}
