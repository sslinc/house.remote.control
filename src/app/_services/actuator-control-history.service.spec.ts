import { TestBed } from "@angular/core/testing";

import { ActuatorControlHistoryService } from "./actuator-control-history.service";

describe("ActuatorControlService", () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it("should be created", () => {
    const service: ActuatorControlHistoryService = TestBed.get(
      ActuatorControlHistoryService
    );
    expect(service).toBeTruthy();
  });
});
