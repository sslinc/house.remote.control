import { TestBed } from '@angular/core/testing';

import { AutoScheduleService } from './auto-schedule.service';

describe('AutoScheduleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AutoScheduleService = TestBed.get(AutoScheduleService);
    expect(service).toBeTruthy();
  });
});
