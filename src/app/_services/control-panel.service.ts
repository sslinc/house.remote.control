import { Injectable } from "@angular/core";
import { NetworkService } from "./network.service";
import { ControlPanel } from "../_models/control-panel";

import { ServerInformation } from "../_classes/server-information";

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { catchError, map, tap } from "rxjs/operators";
import { Observable, of } from "rxjs";
import { MatSnackBar, MatSnackBarConfig } from "@angular/material/snack-bar";
import { WeatherService } from "../_services/weather.service";
import { AutoScheduleService } from "./auto-schedule.service";
import { ActuatorService } from "./actuator.service";

@Injectable({
  providedIn: "root",
})
export class ControlPanelService {
  private controlServerHost = ServerInformation.getControlServerHost();
  private controlPanelUrl = "/control_panel";

  private option: string;
  private currentControlPanel: ControlPanel;
  private controlPanels: ControlPanel[];
  constructor(
    private http: HttpClient,
    private _snackBar: MatSnackBar,
    private weatherService: WeatherService,
    private actuatorService: ActuatorService
  ) {
    this.currentControlPanel = {
      id: 0,
      name: "temp",
      lat: "0",
      lng: "0",
      user_id: 0,
      option: "1",
      manual_schedule_mode_flag: false,
    };
  }

  get CurrentControlPanel() {
    return this.currentControlPanel;
  }
  set CurrentControlPanel(currentControlPanel: ControlPanel) {
    this.currentControlPanel = currentControlPanel;
  }

  changeControlPanel(e) {
    for (let i = 0; i < this.controlPanels.length; i++) {
      if (e == this.controlPanels[i].option)
        this.CurrentControlPanel = this.controlPanels[i];
    }
    // if (e.indexOf("1") != -1) this.CurrentControlPanel = this.controlPanels[0];
    // else if (e.indexOf("2") != -1)
    //   this.CurrentControlPanel = this.controlPanels[1];
    this.weatherService.openSnackBarWithHumidity(
      this.CurrentControlPanel.lat,
      this.CurrentControlPanel.lng
    );
    this.loadActuatorTotalList();
  }

  getControlPanels(): Observable<ControlPanel[]> {
    return this.http
      .get<ControlPanel[]>(`${this.controlServerHost}${this.controlPanelUrl}`, {
        withCredentials: true,
      })
      .pipe(
        catchError(this.handleError<ControlPanel[]>("getControlPanels", []))
      );
  }

  updateManualScheduleModeFlag(
    control_panel_id: number,
    manual_schedule_mode_flag: boolean
  ) {
    return this.http
      .post(
        `${this.controlServerHost}${this.controlPanelUrl}/${control_panel_id}`,
        {
          manual_schedule_mode_flag: manual_schedule_mode_flag,
        },
        {
          withCredentials: true,
        }
      )
      .pipe(catchError(this.handleError("updateAutoSchedule", [])));
  }
  loadActuatorTotalList() {
    this.actuatorService
      .getActuatorList(this.CurrentControlPanel.id)
      .subscribe((result) => {
        this.actuatorService.actuatorList = result;
      });
  }
  public setControlPanels() {
    this.getControlPanels().subscribe((result) => {
      this.controlPanels = result;
      this.option = this.controlPanels[0].option;
      this.CurrentControlPanel = this.controlPanels[0];
      this.weatherService.openSnackBarWithHumidity(
        this.CurrentControlPanel.lat,
        this.CurrentControlPanel.lng
      );
      if (result.length) {
        this.controlPanels = result;
        let controlpanel = Object.assign({}, this.controlPanels[0]);
        this.CurrentControlPanel = controlpanel;
        this.weatherService.setCurrentWeather(
          this.CurrentControlPanel.lat,
          this.CurrentControlPanel.lng
        );
      } else {
        this.controlPanels = [];
      }
      this.loadActuatorTotalList();
    });
  }

  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
