import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { catchError, map, tap } from "rxjs/operators";
import { Observable, of } from "rxjs";
import { User } from "../_models/user";
import { ServerInformation } from "../_classes/server-information";
@Injectable({
  providedIn: "root",
})
export class AccountService {
  private controlServerHost = ServerInformation.getControlServerHost();
  private loginUrl = "/user/validate";

  user: User;
  message: string;
  constructor(private http: HttpClient) {}
  get User() {
    return this.user;
  }
  set User(user: User) {
    this.user = user;
  }
  logIn(account: string, password: string): Observable<User> {
    return this.http
      .post<User>(
        `${this.controlServerHost}${this.loginUrl}`,
        {
          account: account,
          password: password,
        },
        {
          withCredentials: true,
        }
      )
      .pipe(catchError(this.handleError<User>("logIn")));
  }
  logOut() {
    return this.http
      .get(`${this.controlServerHost}/user/logout`, {
        withCredentials: true,
      })
      .pipe(catchError(this.handleError("logOut")));
  }
  setUser() {
    return this.http
      .get<User>(`${this.controlServerHost}/user`, {
        withCredentials: true,
      })
      .pipe(
        map((result) => result[0]),
        catchError(this.handleError<User>("setUser"))
      );
  }

  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      // console.error(error); // log to console instead
      if (error.status == 404) alert("아이디 혹은 패스워드가 잘못되었습니다.");
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
