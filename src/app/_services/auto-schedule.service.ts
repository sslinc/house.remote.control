import { Injectable } from "@angular/core";
import { ServerInformation } from "../_classes/server-information";
import { Observable, of } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { catchError } from "rxjs/operators";
import { AutoSchedule } from "../_models/auto-schedule";

@Injectable({
  providedIn: "root",
})
export class AutoScheduleService {
  private controlServerHost = ServerInformation.getControlServerHost();
  private autoSchedulesUrl = "/schedule/";

  private autoSchedules = new Array();

  constructor(private http: HttpClient) {}

  get AutoSchedules() {
    return this.autoSchedules;
  }
  set AutoSchedules(autoSchedules: AutoSchedule[]) {
    this.autoSchedules = autoSchedules;
  }
  initAutoSchedules() {
    this.autoSchedules = new Array();
  }
  applyAutoSchedule() {
    return this.http
      .get(`${this.controlServerHost}${this.autoSchedulesUrl}refresh`, {
        withCredentials: true,
      })
      .pipe(catchError(this.handleError("applyAutoSchedule", [])));
  }
  deleteAutoSchedule(auto_schedule_id: number) {
    return this.http
      .delete(
        `${this.controlServerHost}${this.autoSchedulesUrl}${auto_schedule_id}`,
        {
          withCredentials: true,
        }
      )
      .pipe(catchError(this.handleError("deleteAutoSchedule", [])));
  }
  updateAutoSchedule(index: number) {
    return this.http
      .post(
        `${this.controlServerHost}${this.autoSchedulesUrl}${this.autoSchedules[index].id}`,
        {
          hour: this.autoSchedules[index].hour,
          minute: this.autoSchedules[index].minute,
          operation: this.autoSchedules[index].operation,
        },
        {
          withCredentials: true,
        }
      )
      .pipe(catchError(this.handleError("updateAutoSchedule", [])));
  }
  putAutoSchedule(actuator_id, hour, minute, operation) {
    return this.http
      .put(
        `${this.controlServerHost}${this.autoSchedulesUrl}${actuator_id}`,
        {
          hour: hour,
          minute: minute,
          operation: operation,
        },
        {
          withCredentials: true,
        }
      )
      .pipe(catchError(this.handleError("updateAutoSchedule", [])));
  }
  getAutoSchedulesForControlPanel(control_panel_id: number) {
    return this.http
      .get<AutoSchedule[]>(
        `${this.controlServerHost}${this.autoSchedulesUrl}${control_panel_id}`,
        {
          withCredentials: true,
        }
      )
      .pipe(
        catchError(this.handleError<AutoSchedule[]>("getAutoSchedules", []))
      );
  }
  getAutoSchedulesForActuator(actuator_id: number) {
    return this.http
      .get<AutoSchedule[]>(
        `${this.controlServerHost}${this.autoSchedulesUrl}actuator/${actuator_id}`,
        {
          withCredentials: true,
        }
      )
      .pipe(
        catchError(this.handleError<AutoSchedule[]>("getAutoSchedules", []))
      );
  }
  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
