import { TestBed } from "@angular/core/testing";

import { AccountService } from "../_services/account.service";

describe("LoginService", () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it("should be created", () => {
    const service: AccountService = TestBed.get(AccountService);
    expect(service).toBeTruthy();
  });
});
