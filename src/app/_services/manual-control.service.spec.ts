import { TestBed } from '@angular/core/testing';

import { ManualControlService } from './manual-control.service';

describe('ManualControlService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManualControlService = TestBed.get(ManualControlService);
    expect(service).toBeTruthy();
  });
});
