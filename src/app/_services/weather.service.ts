import { Injectable } from "@angular/core";
import { ServerInformation } from "../_classes/server-information";
import { MatSnackBar, MatSnackBarConfig } from "@angular/material/snack-bar";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { catchError, map, tap } from "rxjs/operators";
import { Observable, of } from "rxjs";
@Injectable({
  providedIn: "root",
})
export class WeatherService {
  private controlServerHost = ServerInformation.getControlServerHost();
  private humidityUrl = "/weather";
  humidity;
  temperature;
  constructor(private http: HttpClient, private _snackBar: MatSnackBar) {}
  get Humidity() {
    return this.humidity;
  }
  set Humidity(humidity) {
    this.humidity = humidity;
  }
  get Temperature() {
    return this.temperature;
  }
  set Temperature(temperature) {
    this.temperature = temperature;
  }
  openSnackBarWithHumidity(lat, lng) {
    if (this.humidity == undefined) {
      this.getWeather(lat, lng).subscribe((result) => {
        let myObj = JSON.parse(result);
        this.humidity = Number(myObj.current_observation.atmosphere.humidity);
        this.temperature = Number(
          myObj.current_observation.condition.temperature
        );
        this.temperature = (this.temperature - 32) / 1.8;
        this.temperature = this.temperature.toFixed(1);
        if (this.humidity > 85) {
          this.openSnackBar(
            `습도가 ${myObj.current_observation.atmosphere.humidity}%입니다. 날씨를 확인하세요.`
          );
        }
      });
    } else if (this.humidity > 85) {
      this.openSnackBar(`습도가 ${this.humidity}%입니다. 날씨를 확인하세요.`);
    }
  }
  getWeather(lat, lng): Observable<string> {
    return this.http
      .get<string>(
        `${this.controlServerHost}${this.humidityUrl}/lat/${lat}/lng/${lng}`,
        {
          withCredentials: true,
        }
      )
      .pipe(catchError(this.handleError<string>("getHumidity")));
  }
  setCurrentWeather(lat, lng) {
    this.getWeather(lat, lng).subscribe((result) => {
      let myObj = JSON.parse(result);
      this.Humidity = Number(myObj.current_observation.atmosphere.humidity);
      this.temperature = Number(
        myObj.current_observation.condition.temperature
      );
      this.temperature = (this.temperature - 32) / 1.8;
      this.temperature = this.temperature.toFixed(1);
    });
  }
  openSnackBar(message: string, action?: string) {
    this._snackBar.open(message, action, {
      duration: 3000,
    });
  }
  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
