import { Component, OnInit } from "@angular/core";
import { actuators } from "../_models/actuators";
import { ActuatorService } from "../_services/actuator.service";
import { Observable } from "rxjs";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-auto-manual-mode",
  templateUrl: "./auto-manual-mode.component.html",
  styleUrls: ["./auto-manual-mode.component.css"],
})
export class AutoManualModeComponent implements OnInit {
  modules = actuators;
  constructor(
    private actuatorService: ActuatorService,
    private route: ActivatedRoute
  ) {}
  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get("id");
    this.actuatorService.setActuators(id);
  }
  durationChange(id, duration_code, event) {
    let duration = event.target.value;
    if (duration >= 10 && duration <= 400) {
      this.actuatorService
        .updateActuatorDuration(id, event.target.value)
        .subscribe();
    }
  }
  refreshActuators(id: number): void {
    this.actuatorService.setActuators(id);
  }
  ngDoCheck() {}
}
