import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoManualModeComponent } from './auto-manual-mode.component';

describe('AutoManualModeComponent', () => {
  let component: AutoManualModeComponent;
  let fixture: ComponentFixture<AutoManualModeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoManualModeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoManualModeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
