import { Component, OnInit } from "@angular/core";
import { User } from "../_models/user";
import { AccountService } from "../_services/account.service";
import { Router, ActivatedRoute } from "@angular/router";
import { ControlPanelService } from "../_services/control-panel.service";
import { WeatherService } from "../_services/weather.service";

@Component({
  selector: "app-user-state",
  templateUrl: "./user-state.component.html",
  styleUrls: ["./user-state.component.css"],
})
export class UserStateComponent implements OnInit {
  user: User;

  constructor(
    private accountService: AccountService,
    private controlPanelService: ControlPanelService,
    private router: Router,
    private weatherService: WeatherService
  ) {}

  ngOnInit() {
    this.accountService.setUser().subscribe((user) => {
      this.accountService.User = user;
    });
  }

  logOut() {
    this.accountService.logOut().subscribe((result) => {
      this.accountService.User = undefined;
      this.controlPanelService.setControlPanels();
    });
    this.router.navigate(["/total/state"]);
  }
}
