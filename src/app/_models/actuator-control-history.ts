export interface ActuatorControlHistory {
  id: number;
  operation: number;
  house_id: number;
  target: number;
  datetime: Date;
  note: string;
  range: number;
}
