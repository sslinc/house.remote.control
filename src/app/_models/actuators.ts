export const actuators = [
  {
    id: 1,
    name: "천창",
    details: [
      {
        id: 192,
        name: "천창(좌)",
        action: ["열림", "닫힘"],
      },
      {
        id: 194,
        name: "천창(우)",
        action: ["열림", "닫힘"],
      },
    ],
  },
  {
    id: 2,
    name: "측창",
    details: [
      {
        id: 196,
        name: "측창(우상)",
        action: ["열림", "닫힘"],
      },
      {
        id: 198,
        name: "측창(우하)",
        action: ["열림", "닫힘"],
      },
    ],
  },
  {
    id: 3,
    name: "후면창",
    details: [
      {
        id: 200,
        name: "후면창(상)",
        action: ["열림", "닫힘"],
      },
      {
        id: 202,
        name: "후면창(하)",
        action: ["열림", "닫힘"],
      },
    ],
  },
  {
    id: 4,
    name: "2중측창",
    details: [
      {
        id: 204,
        name: "2중측창(우)",
        action: ["열림", "닫힘"],
      },
    ],
  },
  {
    id: 5,
    name: "2중후면창",
    details: [
      {
        id: 206,
        name: "2중후면창",
        action: ["열림", "닫힘"],
      },
    ],
  },
  {
    id: 6,
    name: "3중측커튼",
    details: [
      {
        id: 208,
        name: "3중측커튼(우)",
        action: ["열림", "닫힘"],
      },
      {
        id: 210,
        name: "3중측커튼(후면)",
        action: ["열림", "닫힘"],
      },
    ],
  },
  {
    id: 7,
    name: "유동팬",
    details: [
      {
        id: 212,
        name: "유동팬",
        action: ["작동", "정지"],
      },
    ],
  },
  {
    id: 8,
    name: "환기팬",
    details: [
      {
        id: 213,
        name: "환기팬",
        action: ["작동", "정지"],
      },
    ],
  },
  {
    id: 9,
    name: "수평커튼",
    details: [
      {
        id: 214,
        name: "수평커튼(상)",
        action: ["열림", "닫힘"],
      },
      {
        id: 216,
        name: "수평커튼(하)",
        action: ["열림", "닫힘"],
      },
    ],
  },
];
