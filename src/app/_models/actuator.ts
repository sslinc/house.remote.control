export interface Actuator {
  id: number;
  name: string;
  state: number;
  parent_id: number;
  action: string;
  control_panel_id: number;
  code: number;
  range: number;
  duration: number;
  duration_code: number;
  state_code: number;
}
