export interface ControlPanel {
  id: number;
  name: string;
  lat: string;
  lng: string;
  user_id: number;
  option: string;
  manual_schedule_mode_flag: boolean;
}
