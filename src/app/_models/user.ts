export interface User {
  id: string;
  account: string;
  name: string;
}
