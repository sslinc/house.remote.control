export interface AutoSchedule {
  id: number;
  actuator_id: number;
  hour: number;
  minute: number;
  second: number;
  operation: number;
  user_id: number;
  action: string;
}
