import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoControlModeComponent } from './auto-control-mode.component';

describe('AutoControlModeComponent', () => {
  let component: AutoControlModeComponent;
  let fixture: ComponentFixture<AutoControlModeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoControlModeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoControlModeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
