import { Component, OnInit } from "@angular/core";
import { AutoScheduleService } from "../_services/auto-schedule.service";
import { ControlPanelService } from "../_services/control-panel.service";
import { actuators } from "../_models/actuators";
import { ActuatorService } from "../_services/actuator.service";
@Component({
  selector: "app-auto-control-mode",
  templateUrl: "./auto-control-mode.component.html",
  styleUrls: ["./auto-control-mode.component.css"],
})
export class AutoControlModeComponent implements OnInit {
  private select_actuator = 0;
  private isOpen = true;
  private actuators = actuators;
  private flagShowAddSchedule: boolean = false;
  private addHour;
  private addMinute;
  private addOperation = 0;
  constructor(
    private autoScheduleService: AutoScheduleService,
    private controlPanelService: ControlPanelService,
    private actuatorService: ActuatorService
  ) {}

  clickUpdateAutoSchedule(index: number) {
    this.autoScheduleService.updateAutoSchedule(index).subscribe();
  }
  clickPutAutoSchedule() {
    this.autoScheduleService
      .putAutoSchedule(
        this.select_actuator,
        this.addHour,
        this.addMinute,
        this.addOperation
      )
      .subscribe((result) => this.refreshAutoScheduleForActuator());
  }
  clickApplySchedule() {
    this.autoScheduleService.applyAutoSchedule().subscribe();
  }
  clickAddScheduleDialog() {
    this.flagShowAddSchedule = !this.flagShowAddSchedule;
  }
  clickSearchAutoSchedule() {
    this.refreshAutoScheduleForActuator();
  }
  clickDeleteAutoSchedule(id: number) {
    this.autoScheduleService
      .deleteAutoSchedule(id)
      .subscribe((result) => this.refreshAutoScheduleForActuator());
  }
  selectActuator(e) {
    this.select_actuator = e;
    for (let i = 0; i < this.actuatorService.actuatorList.length; i++) {
      if (this.actuatorService.actuatorList[i].id == this.select_actuator) {
        if (this.actuatorService.actuatorList[i].action == "open") {
          this.isOpen = true;
        } else {
          this.isOpen = false;
        }
      }
    }
  }
  onSelectRange(event) {
    this.addOperation = event.target.value.replace(/[^0-9]/g, "");
  }
  refreshAutoScheduleForActuator() {
    this.autoScheduleService
      .getAutoSchedulesForActuator(this.select_actuator)
      .subscribe((result) => (this.autoScheduleService.AutoSchedules = result));
  }
  ngOnInit() {
    // this.autoScheduleService.initAutoSchedules();
    // this.autoScheduleService.getAutoSchedules(1).subscribe((result) => {
    //   this.autoScheduleService.AutoSchedules = result;
    // });
  }
  ngDoCheck() {}
}
