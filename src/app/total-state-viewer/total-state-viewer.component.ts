import { Component, OnInit } from "@angular/core";
import { ActuatorService } from "../_services/actuator.service";
import { actuators } from "../_models/actuators";
enum operations {
  "정지",
  "작동",
}
@Component({
  selector: "app-total-state-viewer",
  templateUrl: "./total-state-viewer.component.html",
  styleUrls: ["./total-state-viewer.component.css"],
})
export class TotalStateViewerComponent implements OnInit {
  modules = actuators;
  operations = operations;
  constructor(private actuatorService: ActuatorService) {}

  ngOnInit() {}

  refreshActuators(id: number): void {
    this.actuatorService.setActuators(id);
  }
}
