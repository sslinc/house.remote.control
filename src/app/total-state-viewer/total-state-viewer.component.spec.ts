import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalStateViewerComponent } from './total-state-viewer.component';

describe('TotalStateViewerComponent', () => {
  let component: TotalStateViewerComponent;
  let fixture: ComponentFixture<TotalStateViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalStateViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalStateViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
